# Python3.6 with nginx docker image

this repository is Python3.6 with nginx docker image for sample Django2 project.

about sample Django2 project, see [https://gitlab.com/imp555/django2-nginx.git](https://gitlab.com/imp555/django2-nginx.git)

original source, see [https://github.com/sclorg/django-ex.git](https://github.com/sclorg/django-ex.git)


## Usage 

1. `Import YAML/JSON` on your OpenShift WebConsole
2. wait for building image stream on your project(labeled `python3.6-nginx:latest`)
3. build python application, see [https://gitlab.com/imp555/django2-nginx.git](https://gitlab.com/imp555/django2-nginx.git)

## Special files in this repository

```
openshift/         - OpenShift-specific files
  scripts          - helper scripts
  templates        - register template for dockerimage
s2i/bin/
  assemble         - application for deployment
  run              - application for running
  usage            - print the userse of this image.
Dockerfile         - s2i based centos7 Dockerfile.
```

## Note

- if you need set up time_zone(default Asia/Tokyo), please customize Dockerfile.

```
    ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime && \
```

for example:
```
    ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime && \
```
