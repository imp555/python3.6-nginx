# This image provides a Python 3.6 environment you can use to run your Python
# applications.
FROM python:3.6

ENV PYTHON_VERSION=3.6 \
    PATH=$HOME/.local/bin/:$PATH \
    PYTHONUNBUFFERED=1 \
    PYTHONIOENCODING=UTF-8 \
    LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    PIP_NO_CACHE_DIR=off

ENV SUMMARY="Platform for building and running Python $PYTHON_VERSION applications \
with nginx." \
    DESCRIPTION="Python $PYTHON_VERSION available as container is a base platform for \
building and running various Python $PYTHON_VERSION applications and frameworks with nginx."

LABEL summary="$SUMMARY" \
      description="$DESCRIPTION" \
      io.k8s.description="$DESCRIPTION" \
      io.k8s.display-name="Python $PYTHON_VERSION with nginx" \
      io.openshift.expose-services="8000:http" \
      io.openshift.tags="builder,python,python36,rh-python36,nginx" \
      com.redhat.component="python36-container" \
      name="python3.6-nginx" \
      version="1" \
      usage="s2i build https://gitlab.com/imp555/django2-nginx.git your_project_name/python3.6-nginx django2-nginx" \
      maintainer="imp555 <imp555@nethome.ne.jp>"

USER root

RUN INSTALL_PYTHON_PKGS="rh-python36 rh-python36-python-devel rh-python36-python-setuptools rh-python36-python-pip" && \
    INSTALL_NGINX_PKGS="nginx" && \
    yum install -y centos-release-scl && \
    yum install -y http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm && \
    yum -y --setopt=tsflags=nodocs install --enablerepo=centosplus $INSTALL_PYTHON_PKGS && \
    yum -y --setopt=tsflags=nodocs install --enablerepo=nginx $INSTALL_NGINX_PKGS && \
    rpm -V $INSTALL_PYTHON_PKGS $INSTALL_NGINX_PKGS && \
    yum -y clean all --enablerepo='*' && \
    ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime && \
    sed -i.bak 's/listen\(.*\)80;/listen 8000;/' /etc/nginx/conf.d/default.conf && \
    sed -i.bak 's/^user/#user/' /etc/nginx/nginx.conf && \
    nginx -t && \
    systemctl enable nginx

EXPOSE 8000

# Copy the S2I scripts from the specific language image to $STI_SCRIPTS_PATH.
COPY ./s2i/bin/ $STI_SCRIPTS_PATH

# Copy extra files to the image.
COPY ./root/ /


# - Create a Python virtual environment for use by any application to avoid
#   potential conflicts with Python packages preinstalled in the main Python
#   installation.
# - In order to drop the root user, we have to make some directories world
#   writable as OpenShift default security model is to run the container
#   under random UID.

RUN chown -R 1001:1001 /usr/share/nginx && \
    chown -R 1001:1001 /var/log/nginx && \
    touch /var/run/nginx.pid && \
    chown -R 1001:1001 /var/run/nginx.pid && \
    chmod 666 /var/run/nginx.pid && \
    chown -R 1001:1001 /etc/nginx && \
    source scl_source enable rh-python36 && \
    virtualenv ${APP_ROOT} && \
    chown -R 1001:0 ${APP_ROOT} && \
    chown -R 1001:0 ${STI_SCRIPTS_PATH} && \
    chmod -R +x ${STI_SCRIPTS_PATH} && \
    fix-permissions ${APP_ROOT} -P && \
    rpm-file-permissions && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

USER 1001

# Set the default CMD to print the usage of the language image.
CMD $STI_SCRIPTS_PATH/usage
